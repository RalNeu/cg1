#version 330

in vec4 fragColor;
in vec3 fragNormal;

out vec4 outputColor;

void main()
{
    outputColor = fragColor;
}
