#pragma once

#include "core/camera/ArcballCamera.h"
#include "core/camera/FreeCamera.h"
#include <glm/glm.hpp>
#include "core/open_gl.h"

namespace viscom {
    class GPUProgram;
    class FrameBuffer;
    class ApplicationNodeBase;
} // namespace viscom

#define FREE_FLIGHT_CAMERA 0
#define ARCBALL_CAMERA 1

namespace cg1 {

#define CAMERA_MODE ARCBALL_CAMERA

#if CAMERA_MODE == ARCBALL_CAMERA
    using ICGCamera = viscom::ArcballCamera;
#define CURSOR_MODE GLFW_CURSOR_NORMAL
#elif CAMERA_MODE == FREE_FLIGHT_CAMERA
    using CG1Camera = viscom::FreeCamera;
#define CURSOR_MODE GLFW_CURSOR_DISABLED
#endif

    class Scene
    {
    public:
        Scene(viscom::ApplicationNodeBase* app, const viscom::FWConfiguration& config);
        ~Scene();

        void RenderScene(viscom::FrameBuffer& fbo);
        void RenderFractal(viscom::FrameBuffer& fbo);
        void RenderCube(viscom::FrameBuffer& fbo);
        void RenderGUI(viscom::FrameBuffer& fbo);
        void UpdateScene(double currentTime, double elapsedTime, viscom::ApplicationNodeBase* app) noexcept;
        bool KeyboardCallback(int /* key */, int /* scancode */, int /*action */, int /* mods */) { return false; }
        bool CharCallback(unsigned int /* character */, int /* mods */) { return false; }
        bool MouseScrollCallback(double /* xoffset */, double yoffset, viscom::ApplicationNodeBase* app)
        {
            return camera_.HandleMouse(-1, 0, static_cast<float>(yoffset), app);
        };
        bool MouseButtonCallback(int button, int action, viscom::ApplicationNodeBase* app)
        {
            return camera_.HandleMouse(button, action, 0.0f, app);
        };
        bool MousePosCallback(double /* x */, double /* y */, viscom::ApplicationNodeBase* app)
        {
            return camera_.HandleMouse(-1, 0, 0.0f, app);
        };

    private:
        template<typename T> static void CreateVertices(std::vector<T>& vertices) noexcept;
        template<typename T> static void CreateFractalVertices(std::vector<T>& vertices) noexcept;
        template<typename T>
        static std::vector<T> CreateFractalTetrahedra(std::vector<T>& tetrahedra, unsigned int depth) noexcept;
        template<typename T>
        void CreateAndFillVAO(GLuint& vao, GLuint& vbo, const std::array<GLuint, 3>& attribLocations,
                              std::vector<T>& vertices) noexcept;

        static glm::vec3 GenerateNormal(const glm::vec4& a, const glm::vec4& b, const glm::vec4& c);

        /** Holds the camera object. */
        cg1::ICGCamera camera_;
        /** Holds the scenes GPU program. */
        std::shared_ptr<viscom::GPUProgram> program_;
        /** Holds the OpenGL vertex buffer id. */
        GLuint vertexBuffer_;
        /** Holds the OpenGL vertex array object id. */
        GLuint vertexArrayObject_;

        /** Holds the OpenGL vertex buffer id for the fractal. */
        GLuint vertexBufferFractal_;
        /** Holds the OpenGL vertex array object id for the fractal. */
        GLuint vertexArrayObjectFractal_;

        /** Holds uniform name for the model matrix. */
        GLint matModelUniformLocation_;
        /** Holds uniform name for the normal matrix. */
        GLint matNormalUniformLocation_;
        /** Holds uniform name for the model-view-projection matrix. */
        GLint matMVPUniformLocation_;

        /** Holds the model matrix. */
        glm::mat4 modelMatrix_;
        /** Holds the normal matrix. */
        glm::mat4 normalMatrix_;
        /** Holds the MVP matrix. */
        glm::mat4 MVPMatrix_;
        /** Determines which mode to render*/
        bool renderFractal_;
    };
} // namespace cg1