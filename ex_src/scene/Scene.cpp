#include "Scene.h"
#include "app/ApplicationNodeImplementation.h"
#include "core/gfx/FrameBuffer.h"
#include "core/gfx/GPUProgram.h"
#include "core/gfx/Texture.h"
#include "core/gfx/mesh/Mesh.h"
#include "core/gfx/mesh/MeshRenderable.h"
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>

#include <iostream>

namespace cg1 {

    namespace scene {
        constexpr unsigned int MAX_DEPTH = 7;

        /** The vertex object used in these examples. */
        struct Vertex
        {
            /** Standard constructor. */
            Vertex() : position{}, color{}, normal{} {};
            /** Constructor to initialize the vertex. */
            Vertex(const glm::vec4& pos, const glm::vec4& col, const glm::vec3& norm)
                : position{pos}, color{col}, normal{norm} {};
            /** The vertex position. */
            glm::vec4 position;
            /** The vertex color. */
            glm::vec4 color;
            /** The vertex normal. */
            glm::vec3 normal;
        };

        /** Simple tetrahedron with 4 vertices. */
        using Tetrahedron = std::array<Vertex, 4>;
    } // namespace scene

    /**
     *  Constructor.
     */
    Scene::Scene(viscom::ApplicationNodeBase* app, const viscom::FWConfiguration&)
        : camera_{glm::vec3(0.0f, 0.0f, 5.0f), *app->GetCamera()},
          program_{app->GetGPUProgramManager().GetResource(
              "Exercise1Program", std::vector<std::string>{"exercise1.vert", "exercise1Light.frag"})},
          vertexBuffer_{0},
          vertexArrayObject_{0},
          matModelUniformLocation_{-1},
          matNormalUniformLocation_{-1},
          matMVPUniformLocation_{-1},
          modelMatrix_{1.0f},
          normalMatrix_{1.0f},
          MVPMatrix_{1.0f},
          renderFractal_{false}
    {
        std::vector<scene::Vertex> vertices;
        CreateVertices(vertices);

        std::vector<scene::Vertex> verticesFractal;
        CreateFractalVertices(verticesFractal);

        // get the vertex attribute locations from the shader
        std::array<GLuint, 3> attribLocations;
        attribLocations[0] = glGetAttribLocation(program_->getProgramId(), "position");
        attribLocations[1] = glGetAttribLocation(program_->getProgramId(), "color");
        attribLocations[2] = glGetAttribLocation(program_->getProgramId(), "normal");

        // get the uniform locations from the shader
        matModelUniformLocation_ = glGetUniformLocation(program_->getProgramId(), "matModel");
        matNormalUniformLocation_ = glGetUniformLocation(program_->getProgramId(), "matNormal");
        matMVPUniformLocation_ = glGetUniformLocation(program_->getProgramId(), "matMVP");

        CreateAndFillVAO(vertexArrayObject_, vertexBuffer_, attribLocations, vertices);
        CreateAndFillVAO(vertexArrayObjectFractal_, vertexBufferFractal_, attribLocations, verticesFractal);
    }

    /**
     *  Destructor.
     */
    Scene::~Scene()
    {
        if (vertexArrayObject_ != 0) glDeleteVertexArrays(1, &vertexArrayObject_);
        if (vertexBuffer_ != 0) glDeleteBuffers(1, &vertexBuffer_);
    }

    /**
     *  Creates the vertices array.
     *  @param vertices the vector to add the vertices to.
     */
    template<typename T> void Scene::CreateVertices(std::vector<T>& vertices) noexcept
    {
        static_assert(std::is_same<T, scene::Vertex>::value,
                      "Method is only implemented for objects of type cg1::scene::Vertex");

        vertices.clear();

        constexpr GLfloat cubeData[]{-1, -1, -1, -1, -1, 1,  -1, 1,  -1,

                                     -1, 1,  -1, -1, -1, 1,  -1, 1,  1,

                                     1,  1,  -1, 1,  1,  1,  1,  -1, -1,

                                     1,  -1, -1, 1,  1,  1,  1,  -1, 1,

                                     -1, -1, 1,  -1, -1, -1, 1,  -1, -1,

                                     1,  -1, 1,  -1, -1, 1,  1,  -1, -1,

                                     1,  1,  1,  1,  1,  -1, -1, 1,  1,

                                     -1, 1,  1,  1,  1,  -1, -1, 1,  -1,

                                     -1, -1, -1, -1, 1,  -1, 1,  1,  -1,

                                     1,  1,  -1, 1,  -1, -1, -1, -1, -1,

                                     1,  1,  1,  -1, 1,  1,  1,  -1, 1,

                                     -1, 1,  1,  -1, -1, 1,  1,  -1, 1};

        constexpr GLfloat colorData[]{1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0,
                                      1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0};

        for (auto i{0U}; i < 6U; ++i) {
            auto colorIndex{i * 3};
            glm::vec4 color{colorData[colorIndex], colorData[colorIndex + 1], colorData[colorIndex + 2], 1.0f};

            for (auto j{0U}; j < 2U; ++j) {
                auto index{((i * 2U) + j) * 9U};

                glm::vec4 a{cubeData[index], cubeData[index + 1], cubeData[index + 2], 1.0f};
                glm::vec4 b{cubeData[index + 3], cubeData[index + 4], cubeData[index + 5], 1.0f};
                glm::vec4 c{cubeData[index + 6], cubeData[index + 7], cubeData[index + 8], 1.0f};

                auto n{GenerateNormal(a, b, c)};

                vertices.emplace_back(a, color, n);
                vertices.emplace_back(b, color, n);
                vertices.emplace_back(c, color, n);
            }
        }
    }

    glm::vec3 Scene::GenerateNormal(const glm::vec4& a, const glm::vec4& b, const glm::vec4& c)
    {
        glm::vec3 dir1 = glm::vec3(b) - glm::vec3(a);
        glm::vec3 dir2 = glm::vec3(c) - glm::vec3(a);
        glm::vec3 cp = glm::cross(dir1, dir2);
        return glm::normalize(cp);
    }

    /**
     *  Creates the vertices array for the fractal.
     *  @param vertices the vector to add the vertices to.
     */
    template<typename T> void Scene::CreateFractalVertices(std::vector<T>& vertices) noexcept
    {
        static_assert(std::is_same<T, scene::Vertex>::value,
                      "Method is only implemented for objects of type cg1::scene::Vertex");

        scene::Tetrahedron initTetrahedron;

        glm::vec4 color{1.0, 1.0, 1.0, 1.0};
        scene::Vertex top = scene::Vertex(glm::vec4(0.0, 1.0, 0.0, 1.0), glm::vec4(1.0, 0.0, 0.0, 1.0), glm::vec3());
        scene::Vertex frontLeft =
            scene::Vertex(glm::vec4(-1.0, -1.0, -1.0, 1.0), glm::vec4(0.0, 1.0, 0.0, 1.0), glm::vec4());
        scene::Vertex frontRight =
            scene::Vertex(glm::vec4(1.0, -1.0, -1.0, 1.0), glm::vec4(0.0, 0.0, 1.0, 1.0), glm::vec4());
        scene::Vertex back = scene::Vertex(glm::vec4(0.0, -1.0, 1.0, 1.0), glm::vec4(1.0, 1.0, 1.0, 1.0), glm::vec4());

        initTetrahedron[0] = top;
        initTetrahedron[1] = frontRight;
        initTetrahedron[2] = frontLeft;
        initTetrahedron[3] = back;

        std::vector<scene::Tetrahedron> initTetrahedronList{std::move(initTetrahedron)};
        std::vector<scene::Tetrahedron> tetrahedronList = CreateFractalTetrahedra(initTetrahedronList, 0);

        vertices.clear();
        // add all tetrahedron to the vertex list
        for (const auto& th : tetrahedronList) {
            // TODO: set correct normals.
            glm::vec3 normal{GenerateNormal(th[0].position, th[1].position, th[2].position)};
            vertices.push_back(th[0]);
            vertices.back().normal = normal;
            vertices.push_back(th[1]);
            vertices.back().normal = normal;
            vertices.push_back(th[2]);
            vertices.back().normal = normal;

            normal = GenerateNormal(th[0].position, th[2].position, th[3].position);
            vertices.push_back(th[0]);
            vertices.back().normal = normal;
            vertices.push_back(th[2]);
            vertices.back().normal = normal;
            vertices.push_back(th[3]);
            vertices.back().normal = normal;

            normal = GenerateNormal(th[0].position, th[3].position, th[1].position);
            vertices.push_back(th[0]);
            vertices.back().normal = normal;
            vertices.push_back(th[3]);
            vertices.back().normal = normal;
            vertices.push_back(th[1]);
            vertices.back().normal = normal;

            normal = GenerateNormal(th[1].position, th[3].position, th[2].position);
            vertices.push_back(th[1]);
            vertices.back().normal = normal;
            vertices.push_back(th[3]);
            vertices.back().normal = normal;
            vertices.push_back(th[2]);
            vertices.back().normal = normal;
        }
    }

	scene::Vertex getMiddle(scene::Vertex& first, scene::Vertex& second) {
        auto position = (first.position + second.position) / 2.0f;
        auto color = (first.color + second.color) / 2.0f;
		

        return scene::Vertex(position, color, first.normal);
	}

    template<typename T>
    std::vector<T> Scene::CreateFractalTetrahedra(std::vector<T>& tetrahedra, unsigned int depth) noexcept
    {
        static_assert(std::is_same<T, scene::Tetrahedron>::value,
                      "Method is only implemented for objects of type cg1::scene::Tetrahedron");

        std::vector<scene::Tetrahedron> result;
        for (const auto& th : tetrahedra) {
            auto t{th[0]};
            auto r{th[1]};
            auto l{th[2]};
            auto b{th[3]};

            std::array<scene::Tetrahedron, 4> newThs;

			
			scene::Vertex tl = getMiddle(t, l);
            scene::Vertex tr = getMiddle(t, r);
            scene::Vertex tb = getMiddle(t, b);
            scene::Vertex lb = getMiddle(l, b);
            scene::Vertex lr = getMiddle(l, r);
            scene::Vertex rb = getMiddle(r, b);


			newThs[0][0] = t;
            newThs[0][1] = tr;
            newThs[0][2] = tl;
            newThs[0][3] = tb;

            newThs[1][0] = tl;
            newThs[1][1] = lr;
            newThs[1][2] = l;
            newThs[1][3] = lb;

            newThs[2][0] = tr;
            newThs[2][1] = r;
            newThs[2][2] = lr;
            newThs[2][3] = rb;

            newThs[3][0] = tb;
            newThs[3][1] = rb;
            newThs[3][2] = lb;
            newThs[3][3] = b;
			
            // add newly created tetrahedra to the list
            result.insert(result.end(), newThs.cbegin(), newThs.cend());
        }

        if (depth < scene::MAX_DEPTH)
            return CreateFractalTetrahedra(result, depth + 1);
        else
            return result;
    }

    /**
     *  Creates a vertex array object and vertex buffer for vertices.
     *  @param vao the vertex array object to create.
     *  @param vbo the vertex buffer object to create.
     *  @param vertices the vertices to fill the buffer with.
     */
    template<typename T>
    void Scene::CreateAndFillVAO(GLuint& vao, GLuint& vbo, const std::array<GLuint, 3>& attribLocations,
                                 std::vector<T>& vertices) noexcept
    {
        static_assert(std::is_same<T, scene::Vertex>::value,
                      "Method is only implemented for objects of type cg1::scene::Vertex");

        // create vertex array object and vertex buffer
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        // fill vertex buffer
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(scene::Vertex),
                     reinterpret_cast<GLvoid*>(vertices.data()), GL_STATIC_DRAW);

        // set vertex attributes
        glEnableVertexAttribArray(attribLocations[0]);
        glVertexAttribPointer(attribLocations[0], 4, GL_FLOAT, GL_FALSE, sizeof(scene::Vertex), nullptr);
        glEnableVertexAttribArray(attribLocations[1]);
        glVertexAttribPointer(attribLocations[1], 4, GL_FLOAT, GL_FALSE, sizeof(scene::Vertex),
                              reinterpret_cast<GLvoid*>(sizeof(glm::vec4)));
        glEnableVertexAttribArray(attribLocations[2]);
        glVertexAttribPointer(attribLocations[2], 3, GL_FLOAT, GL_FALSE, sizeof(scene::Vertex),
                              reinterpret_cast<GLvoid*>(sizeof(glm::vec4) + sizeof(glm::vec4)));

        // finish vertex array object setup
        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    /**
     *  Updates the current scene.
     *  @param currentTime the current application time.
     *  @param elapsedTime the time elapsed during the last frame.
     *  @param app the ApplicationNodeBase needed to update the Camera
     */
    void Scene::UpdateScene(double, double elapsedTime, viscom::ApplicationNodeBase* app) noexcept
    {
        camera_.UpdateCamera(elapsedTime, app);
    }

    /**
     *  Renders the scene.
     *  @param fbo the FrameBuffer used to render the Scene into
     */
    void Scene::RenderScene(viscom::FrameBuffer& fbo)
    {
        if (!renderFractal_) {
            RenderCube(fbo);
        } else {
            RenderFractal(fbo);
        }
    }

    /**
     * Renders the Cube
     * @param fbo the FrameBuffer used to render the Scene into
     */
    void Scene::RenderCube(viscom::FrameBuffer& fbo)
    {
        auto VPMatrix_ = camera_.GetViewProjMatrix();
        MVPMatrix_ = VPMatrix_ * modelMatrix_;

        fbo.DrawToFBO([this]() {
            glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(modelMatrix_));

            glUseProgram(program_->getProgramId());
            glUniformMatrix4fv(matModelUniformLocation_, 1, GL_FALSE, glm::value_ptr(modelMatrix_));
            glUniformMatrix3fv(matNormalUniformLocation_, 1, GL_FALSE, glm::value_ptr(normalMatrix));
            glUniformMatrix4fv(matMVPUniformLocation_, 1, GL_FALSE, glm::value_ptr(MVPMatrix_));

            glBindVertexArray(vertexArrayObject_);
            glDrawArrays(GL_TRIANGLES, 0, 36); // 36 = 6 Sides * 2 Triangles * 3 Vertices
            glBindVertexArray(0);
            glUseProgram(0);
        });
    }

    /**
     *  Renders the fractal.
     *  @param fbo the FrameBuffer used to render the Scene into
     */
    void Scene::RenderFractal(viscom::FrameBuffer& fbo)
    {
        auto VPMatrix_ = camera_.GetViewProjMatrix();
        MVPMatrix_ = VPMatrix_ * modelMatrix_;

        fbo.DrawToFBO([this]() {
            auto normalMatrix = glm::inverseTranspose(modelMatrix_);

            glUseProgram(program_->getProgramId());
            glUniformMatrix4fv(matModelUniformLocation_, 1, GL_FALSE, glm::value_ptr(modelMatrix_));
            glUniformMatrix3fv(matNormalUniformLocation_, 1, GL_FALSE, glm::value_ptr(normalMatrix));
            glUniformMatrix4fv(matMVPUniformLocation_, 1, GL_FALSE, glm::value_ptr(MVPMatrix_));

            glBindVertexArray(vertexArrayObjectFractal_);
            glDrawArrays(GL_TRIANGLES, 0,
                         static_cast<GLsizei>(glm::pow(4, (scene::MAX_DEPTH + 1))) * 4
                             * 3); // TODO: Change number of Vertices for Fractal Rendering
            glBindVertexArray(0);
            glUseProgram(0);
        });
    }

    /**
     *  Renders the ImGui content.
     *  @param fbo the FrameBuffer used to render the GUI into
     */
    void Scene::RenderGUI(viscom::FrameBuffer& fbo)
    {
        fbo.DrawToFBO([this]() {
            ImGui::SetNextWindowPos(ImVec2(10, 10), ImGuiSetCond_FirstUseEver);
            ImGui::SetNextWindowSize(ImVec2(500, 200), ImGuiSetCond_FirstUseEver);
            ImGui::Begin("Render Parameters");
            ImGui::Checkbox("Render Fractal", reinterpret_cast<bool*>(&renderFractal_));
            ImGui::End();
        });
    }
} // namespace cg1
