#version 330

in vec4 position;
in vec4 color;
in vec3 normal;

uniform mat4 matModel;
uniform mat3 matNormal;
uniform mat4 matMVP;

out vec4 fragColor;
out vec3 fragNormal;

void main()
{
    fragColor = color;
    fragNormal = matNormal * normal;
    gl_Position = matMVP * position;
    
}
