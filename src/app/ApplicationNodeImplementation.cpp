/**
 * @file   ApplicationNodeImplementation.cpp
 * @author Sebastian Maisch <sebastian.maisch@uni-ulm.de>
 * @date   2016.11.30
 *
 * @brief  Implementation of the application node class.
 */

#include "ApplicationNodeImplementation.h"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

namespace viscom {

    ApplicationNodeImplementation::ApplicationNodeImplementation(ApplicationNodeInternal* appNode)
        : ApplicationNodeBase{appNode}
    {
        SetCursorInputMode(CURSOR_MODE);

        glFrontFace(GL_CW);
        glCullFace(GL_BACK);
        glEnable(GL_CULL_FACE);
        glDepthFunc(GL_LEQUAL);
        glEnable(GL_DEPTH_TEST);

        scene_ = std::make_unique<cg1::Scene>(this, GetConfig());
    }

    ApplicationNodeImplementation::~ApplicationNodeImplementation() = default;

    void ApplicationNodeImplementation::UpdateFrame(double currentTime, double elapsedTime)
    {
        scene_->UpdateScene(currentTime, elapsedTime, this);
    }

    void ApplicationNodeImplementation::ClearBuffer(FrameBuffer& fbo)
    {
        fbo.DrawToFBO([]() {
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            glClearDepth(1.0);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        });
    }

    void ApplicationNodeImplementation::DrawFrame(FrameBuffer& fbo) { scene_->RenderScene(fbo); }

    bool ApplicationNodeImplementation::KeyboardCallback(int key, int scancode, int action, int mods)
    {
        if (ApplicationNodeBase::KeyboardCallback(key, scancode, action, mods)) return true;
        return scene_->KeyboardCallback(key, scancode, action, mods);
    }

    bool ApplicationNodeImplementation::CharCallback(unsigned int character, int mods)
    {
        if (ApplicationNodeBase::CharCallback(character, mods)) return true;
        return scene_->CharCallback(character, mods);
    }

    bool ApplicationNodeImplementation::MouseButtonCallback(int button, int action)
    {
        if (ApplicationNodeBase::MouseButtonCallback(button, action)) return true;
        return scene_->MouseButtonCallback(button, action, this);
    }

    bool ApplicationNodeImplementation::MousePosCallback(double x, double y)
    {
        if (ApplicationNodeBase::MousePosCallback(x, y)) return true;
        return scene_->MousePosCallback(x, y, this);
    }

    bool ApplicationNodeImplementation::MouseScrollCallback(double xoffset, double yoffset)
    {
        if (ApplicationNodeBase::MouseScrollCallback(xoffset, yoffset)) return true;
        return scene_->MouseScrollCallback(xoffset, yoffset, this);
    }

} // namespace viscom
