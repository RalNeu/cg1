#version 330

in vec4 fragColor;
in vec3 fragNormal;

out vec4 outputColor;

void main()
{
    const vec3 lightDir = normalize(vec3(0.2f, 0.4f, -1.0f));
    outputColor = (0.8 * fragColor * clamp(dot(normalize(fragNormal), lightDir), 0.0f, 1.0f)) + (0.2 * fragColor);
}
