list(APPEND EXERCISE_INCLUDE_DIRS 
	ex_src)

add_custom_target(submission)

# Copy submission Files
 	add_custom_command(
        TARGET submission POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory
                ${CMAKE_SOURCE_DIR}/ex_src
                ${CMAKE_SOURCE_DIR}/submission/ex_src)

    add_custom_command(
        TARGET submission POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory
                ${CMAKE_SOURCE_DIR}/resources
                ${CMAKE_SOURCE_DIR}/submission/resources)