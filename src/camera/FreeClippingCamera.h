//
// Created by alex on 30.08.17.
//

#pragma once

#include "core/camera/CameraBase.h"
#include "core/camera/FreeCamera.h"

namespace viscom {
    class FreeClippingCamera final : public CameraBase
    {
    public:
        FreeClippingCamera(const glm::vec3& camPos, viscom::CameraHelper& cameraHelper) noexcept;

        virtual bool HandleMouse(int button, int action, float mouseWheelDelta,
                                 const ApplicationNodeBase* sender) override;
        virtual void UpdateCamera(double elapsedTime, const ApplicationNodeBase* sender) override;
        /** Returns a vector of line segment vertices. */
        const std::vector<glm::vec4> getFrustumLineSegmentsInWorldspace() const;

    private:
        FreeCamera freeCamera_;
    };
} // namespace viscom