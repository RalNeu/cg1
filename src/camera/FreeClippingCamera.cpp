//
// Created by alex on 30.08.17.
//

#include "FreeClippingCamera.h"
#include "app/ApplicationNodeImplementation.h"
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

namespace viscom {

    /**
     *  Constructor.
     *  @param theCamPos the cameras initial position.
     *  @param cameraHelper the camera helper class.
     */
    FreeClippingCamera::FreeClippingCamera(const glm::vec3& theCamPos, viscom::CameraHelper& cameraHelper) noexcept
        : CameraBase(theCamPos, cameraHelper), freeCamera_{theCamPos, cameraHelper}
    {
    }

    bool FreeClippingCamera::HandleMouse(int button, int action, float mouseWheelDelta,
                                         const ApplicationNodeBase* sender)
    {
        return freeCamera_.HandleMouse(button, action, mouseWheelDelta, sender);
    }

    /**
     *  Updates the camera parameters using the internal arc-ball.
     */
    void FreeClippingCamera::UpdateCamera(double elapsedTime, const ApplicationNodeBase* sender)
    {
        freeCamera_.UpdateCamera(elapsedTime, sender);
    }

    const std::vector<glm::vec4> FreeClippingCamera::getFrustumLineSegmentsInWorldspace() const
    {
        std::vector<glm::vec4> frustum_points;

        frustum_points.emplace_back(glm::vec4(1.0f, -1.0f, -1.0f, 1.0f));  // near right bottom
        frustum_points.emplace_back(glm::vec4(1.0f, 1.0f, -1.0f, 1.0f));   // near right top
        frustum_points.emplace_back(glm::vec4(-1.0f, 1.0f, -1.0f, 1.0f));  // near left top
        frustum_points.emplace_back(glm::vec4(-1.0f, -1.0f, -1.0f, 1.0f)); // near left bottom

        frustum_points.emplace_back(glm::vec4(1.0f, -1.0f, 1.0f, 1.0f));  // far right bottom
        frustum_points.emplace_back(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));   // far right top
        frustum_points.emplace_back(glm::vec4(-1.0f, 1.0f, 1.0f, 1.0f));  // far left top
        frustum_points.emplace_back(glm::vec4(-1.0f, -1.0f, 1.0f, 1.0f)); // far left bottom

        glm::mat4 invVP = glm::inverse(freeCamera_.GetViewProjMatrix());

        for (auto& p : frustum_points) {
            p = invVP * p;
            p /= p.w;
        }

        // add verticies to draw frustum
        std::vector<glm::vec4> frustum_line_vertices;

        for (int i = 0; i < 4; i++) {
            // near plane lines
            frustum_line_vertices.push_back(frustum_points[i]);
            frustum_line_vertices.push_back(frustum_points[(i + 1) % 4]);

            // far plane lines
            frustum_line_vertices.push_back(frustum_points[4 + i]);
            frustum_line_vertices.push_back(frustum_points[4 + ((i + 1) % 4)]);
        }

        // right plane lines
        frustum_line_vertices.push_back(frustum_points[1]);
        frustum_line_vertices.push_back(frustum_points[5]);
        frustum_line_vertices.push_back(frustum_points[0]);
        frustum_line_vertices.push_back(frustum_points[4]);

        // left plane lines
        frustum_line_vertices.push_back(frustum_points[3]);
        frustum_line_vertices.push_back(frustum_points[7]);
        frustum_line_vertices.push_back(frustum_points[2]);
        frustum_line_vertices.push_back(frustum_points[6]);

        return frustum_line_vertices;
    }
} // namespace viscom
